##Say "hey" to antiSMASH.. again!
I'm Mariana, I'm portuguese and this is my second **Google Summer of Code**.

I'm enrolled in a Bioinformatics and Computational Biology master. I'm graduated in Biochemistry with a Masters in Biopharmaceutical Sciences.
This year I'll be working again with Kai Blin and Marc Chevrette on [antiSMASH](http://antismash.secondarymetabolites.org). 



__My project: antiSMASH Migration to Template System__

antiSMASH, a python-based tool to mine micro-organisms genomes, currently uses pyQuery to generate static HTML pages. Consequently, several difficulties are found when improvements/changes need to be performed. This library is commonly described as too verbose, implying most of the times, string concatenations and mixed code for both presentation and logic. Subsequently, it is not much intuitive. In addition, recent patch update from 1.2.9 to 1.2.10 resulted in a backward compatibility break, and as a minor patch should not raise compatibility issues, the trust in this library is somehow compromised. Template Engines overcome the majority of the issues related to output generation. They are powerful tools that allow the developers to speed up their work in an easier and much cleaner way.

This project proposes moving antiSMASH to jinja2, a well known, widely used and developer-friendly templating language for Python. With this migration, we intend to facilitate the future development of antiSMASH as well as the onboarding of new developers to the team. Moreover, considering jinja2 enormous user-base and applications, a backwards compatibility break in a minor version is unlikely.
 
Welcome to my GSoC Blog 2.0 ^^


##[Week1](https://bitbucket.org/mnave/gsoc-blog-2.0/src/bd262a442aa56ba075f481e11b565fc8bb32127a/posts/week1.md?at=master&fileviewer=file-view-default)


##[Week2](https://bitbucket.org/mnave/gsoc-blog-2.0/src/05fbf88c2aa4f12af1fea836377cee615020edbc/posts/week2.md?at=master&fileviewer=file-view-default)


## [Weeks 3 & 4](https://bitbucket.org/mnave/gsoc-blog-2.0/src/8d216b9c2c3f9860e4d15722b78fdc6dec87b940/posts/weeks%203%20&%204.md?at=master&fileviewer=file-view-default)


## [Weeks 5 & 6](https://bitbucket.org/mnave/gsoc-blog-2.0/src/2c2d66bb1dfbcfcb3ced91c574568c4f34aaacf5/posts/weeks%205%20&%206.md?at=master&fileviewer=file-view-default)


## [Weeks 7 & 8](https://bitbucket.org/mnave/gsoc-blog-2.0/src/fbafa92b5e0c70f91b8035188e7d60ac32790fd1/posts/weeks%207%20&%208.md?at=master&fileviewer=file-view-default)


##[Week 9](https://bitbucket.org/mnave/gsoc-blog-2.0/src/bd28cbeb69eeea0054a9966596a26c9dcad1c372/posts/week9.md?at=master&fileviewer=file-view-default)


##[Week 10](https://bitbucket.org/mnave/gsoc-blog-2.0/src/b7ba76aa81649ea6d1e0f77cfcd489c7fd6476f9/posts/week10.md?at=master&fileviewer=file-view-default)


##[Week 11](https://bitbucket.org/mnave/gsoc-blog-2.0/src/b7ba76aa81649ea6d1e0f77cfcd489c7fd6476f9/posts/week11.md?at=master&fileviewer=file-view-default)