Hello from Portugal (again!)

This week passed by so fast!
Most of my time was working around some minor fixes in the htmls, specially in the sidepanels. 
One of the errors I've made with relative frequency was to forget to close some html tags. 

_why?_
well, when you're reading html that is supposed to be rendered with pyquery, is easy to fail some.

```python

        _alt_weights = _get_alternative_weights(cp)
        if _alt_weights:
            inner_dl = pq('<dl>')
            inner_dt = pq('<dt>')
            inner_dt.text('Alternative weights')
            inner_dl.append(inner_dt)
            inner_dd = pq('<dd>')
            inner_dd.addClass('alt-weight-desc')
            inner_dd.text('(assuming N unmodified Ser/Thr residues)')
            inner_dl.append(inner_dd)
            i = 1
            for weight in _alt_weights:
                inner_dd = pq('<dd>')
                weight_span = pq('<span>')
                weight_span.text('%0.1f Da' % weight)
                weight_span.addClass('alt-weight')
                n_span = pq('<span>')
                n_span.text('N = %d' % i)
                n_span.addClass('alt-weight-n')
                inner_dd.append(weight_span)
                inner_dd.append(n_span)
                inner_dl.append(inner_dd)
                i += 1
            dd.append(inner_dl)
        details_list.append(dd)
```

Don't you agree?

I know.. these missing tags could have been avoided if my editor could close automatically a tag as soon as I open one.
But it's okay - part of an intensive focus training ^^ 

This week was the beginning of the tests and so far, I can't say much about then because I spent 1 day thinking about the "hows" and another one trying to 
1) get internet on Docker and 2) install everything I need.
The problem was: I didn't have enough space on disk to build again.. and just when I've started making some experiments with tests (1st thing: 
run all the tests already done), I felt like my cat on my laptop. I found some shortcuts that closed/crashed Docker.
So for 4 ou 5 times, I had to install everything again ^^

In a few days you'll hear from me.
Mariana