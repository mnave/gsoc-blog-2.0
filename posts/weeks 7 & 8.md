Hello you!

*What did I do during weeks 7 and 8?*

During these two last weeks I've worked on the migration of the **nrpspks** and **sactipeptides** modules to jinja. After all, the latter was pretty easy when compared to any other module. 
On the other hand, the *nrpspks* was a bit painfull - as I thought it would be and wrote about it in my last post.


NRPSPKS -  nonribosomal peptide synthetase (NRPS) and polyketide synthase (PKS) 
This module is huge, comparing to others. The data structure also had to be different - this is a point to revisit and probably change next week.
Had a few problems with the logic behind the rendering. This is one of the problems of pyquery - very verbose.

Along these days, I also stumbled on some bugs which I've been fixing. 
Another thing to notice: I've wrote once about the classes I've created to record, cluster and options. They are now in a _layers.py_ file. 
On previous posts didn't write  much about my solution with the plugins, so here we go.


In this, I've created a lot of properties like

```python

    @property
    def idx(self):
        return self.cluster['idx']
		
```

This allows anyone to get the 'idx' of a cluster, by typing **self.idx**
To ease the migration, I've kept `self.cluster['idx']` along the code.
Here is an example regarding cluster start position, comparing to the end position:

```python
def find_core_peptides(self):
        """Find CDS_motifs containing sactipeptide core peptide annotations"""
        motifs = []

        for motif in utils.get_all_features_of_type(self.record.seq_record, 'CDS_motif'):
            if motif.location.start < self.start or \
               motif.location.end > self.cluster['end']:
                continue
				
```


For each plugin, as I mentioned previosly, I've created a jinja environment. 
What I didn't tell you about each plugin is that each one will define a class that extends ClusterLayer, which means that it inherits all its methods and properties. 
This class will then contain the methods that are exclusive to each cluster type. 
Consider for example, the sactipeptides module:

```python
class SactipeptideLayer(ClusterLayer):
    def __init__(self, cluster, record):
        ClusterLayer.__init__(self, cluster, record)
        self.leader_peptides = self.find_leader_peptides()
        self.core_peptides = self.find_core_peptides()
    ...
```

So in the next weeks, I'll be working with refactorizations, bug fixes and other improvements.

See you ^^ 
		  