First week >> check!


I worked a bit on the "whats" and "hows" and started changing the results' overview page. In the output example on antismash web site we can see this:

[![out_example.jpg](https://s9.postimg.org/4lft5mc1b/out_example.jpg)](https://postimg.org/image/irvk0umwb/)

and already with jinja we have something quite similar:

[![jinja_example.png](https://s16.postimg.org/dc6tykg4l/jinja_example.png)](https://postimg.org/image/wu1heid29/)

cool huh? ^^

But don't get too excited! If you look closer, we still have work to do. And this is __only__ the overview page!
So this week I started porting one specific plugin.. but have to change my approach. So I'll continue to migrate everything that is common to all the generated htmls first.
This will be a long road ^^


See you next week!

