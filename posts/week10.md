Hello there!

Let me start by apologyzing for the late post.
Last time I wrote, I mentioned something about some new features on the site.
Yep, small but one of them was a bit painfull ^^

So now, you can have the nucleotide sequence of a gene/domain directly from the svg on the page and copy it to the clipboard, just like the "AA sequence".

[![nucleotide_seq.jpg](https://s28.postimg.org/o5ck7a471/nucleotide_seq.jpg)](https://postimg.org/image/xpw6u5tix/)


[![nucleotide_seq_domain.jpg](https://s28.postimg.org/msuv5e6rh/nucleotide_seq_domain.jpg)](https://postimg.org/image/8z6igce61/)

Another extra, is the display of the KR stereochemistry prediciton in the sidepanel.

[![kr_stereo_all.jpg](https://s28.postimg.org/uzmuwywu5/kr_stereo_all.jpg)](https://postimg.org/image/c7azte0ft/)

The hardest part in getting this done, was to discover "the link" between python and the page JavaScript. Took me long time to understand what (and how) the data was being passed to the JS.
But mission accomplished ! ^^

Cheers,
Mariana Nave
