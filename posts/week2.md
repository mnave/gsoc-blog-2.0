Hello all!

A bit more than 2 weeks already gone.
The work is being amazing:

* The html is now separated in different small parts. On the main html file, for example, one can see
           
           <body>
              <div id="header">
                 {% include 'top_header.html' %}
                  ...
                 {% include 'footer.html' %} 
	        ...
          </body>
        </html>

this **include** statement is quite simple and amazing.

* the JS is already in their final place
* the legend entry on the results' page is already being properly rendered. To this, I used [macros](http://jinja.pocoo.org/docs/2.9/templates/#macros).
Example:

         {% macro legend(css_class, description) -%}
         <div>
          <div class="legend-field {{css_class}}"></div>
          <div class="legend-label">{{description}}</div>
          </div>
         {%- endmacro %}

Did you notice the "-"? They allow you to control the whitspaces. ^^

* cluster, subcluster and knowncluster blast results are also ported 

* and finally I added what was missing regarding hybrid clusters!

[![jnj_overview.jpg](https://s4.postimg.org/t1ebzl24d/jnj_overview.jpg)](https://postimg.org/image/a92gw05q1/)

[![last_jnj.jpg](https://s9.postimg.org/6kmejl2e7/last_jnj.jpg)](https://postimg.org/image/h7g7p0ajf/)

See you next week ^^



