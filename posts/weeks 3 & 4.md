Hey you!

*What did I do during week3 and 4?*

I started my 3rd GSoC week around the plugin system. 
antiSMASH has different plugins and I'm still not sure how to port them to a template system.
So, somewhere along week 3, I decided to change my approach and try not to get stuck: Instead of focusing on that question, I decided to start rendering, in a "dumb" way, one specific module (results from the thiopeptides plugin).

why "dumb" way? 
well, antismash has these plugins right? They are all in their 'specific_analysis' folder. As a plugin, that folder contains everything it needs to run.
With jinja, we create an **Environment** to further load our templates, so we also have a **loader** . For those who don't know, something like this:

```
from jinja2 import Environment, PackageLoader, select_autoescape
env = Environment(
    loader=PackageLoader('yourapplication', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)
```

We create our env which will load our html templates from the 'templates' folder.
Okay, so far so good.
But to maitain the logic/flow of the pipeline, we cannot have the templates in the 'templates' folder, otherwise we create a dependency, right?

So after this question, I decided to leave this and focus on a way to start seeing some results. Basically, I created all the html templates for the thiopeptides' module and migrated all the methods we need to get the information to be rendered to our "main" python file.
This way, it only works for thiopeptides, but at least I already have the "heavy" html work done > just need to migrate these files to their final places.

This is what I have for a thiopeptide cluster ^^

[![thio.jpg](https://s16.postimg.org/6ff0guz1h/thio.jpg)](https://postimg.org/image/oi8382uw1/)

Next step: play around the plugin system - here we go!


See you next week ^^


