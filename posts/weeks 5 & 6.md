Hello there!

These two last weeks were very productive.
On my last post, one could read about the doubts around the plugin system migration.
How did we solve it?

We've created a jinja environment (and its own loader) for each plugin.
So we have a plugin and its html generation logic is kept on the *html_output.py*. The templates to be rendered can be found in a folder *templates*, where jinja loader will get them.
```
Lassopeptides
    |
	|-templates
	|       |-details.html
	|    	|-sidepanel.html
	|
	|-html_output.py	
	|-__init__.py
	      ...
```	  

Advantages?
We can keep the antiSMASH logic, we avoid major problems with hybrid clusters AND this allows to choose whatever lib/methods to create the html for each plugin. 

In addition to this, we already have some plugins migrated to jinja.
The most complicated ones are on the way!


See you ^^
